import random


class Quiz:
    def __init__(self):
        print("Welcome to AskPython Quiz")
        self.startgame()

    def startgame(self):
        start_answer = input("Are you ready to play the Quiz ? (yes/no): ")
        if start_answer == "yes":
            self.initiate()
        else:
            exit("Exited")

    def initiate(self):
        ques = ["What is your Favourite programming language? ",
                "Do you follow any author on AskPython? ",
                "What is the name of your favourite website for learning Python? "]
        ans = ["python", "yes", "askpython"]
        ans_count = 0
        for _ in range(len(ques)):
            random_question = random.choice(ques)
            answer_input = input(random_question).lower()
            answer_index = ques.index(random_question)
            ans_original = ans[answer_index]
            ques.remove(random_question)
            ans.remove(ans_original)
            if answer_input.lower() == ans_original.lower():
                print("Very good!")
                ans_count += 1
        score = 0
        if ans_count == 0:
            score = 0
        elif ans_count == 3:
            score = 100
        else:
            score = 50
        print("Thankyou for Playing this small quiz game, you attempted ",
              ans_count, " questions correctly!")
        print("Marks Obtained ", score)
        print("BYE")


user = Quiz()
