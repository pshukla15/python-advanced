import csv

sales_tax = 18
for_new = []
file_name = 'data.csv'


def calcprice(sales_tax,file_name,for_new):
    
    with open(file_name,'r') as csv_file:
        csv_reader = csv.DictReader(csv_file)

        for row in csv_reader:
            final_price = ((int(row['Product-CostPrice']) * sales_tax ) //100) + int(row['Product-CostPrice'])
            # print(final_price)

            for_new.append({
                'Product-Name' : row['Product-Name'],
                'Product-CostPrice': row['Product-CostPrice'],
                'Product-SalesTax' : sales_tax,
                'Product-FinalPrice': str(final_price),
                'Country' : row['Country']
            })

    # print(for_new)

    with open('output.csv','w', newline='') as newnew:
        fieldnames = ['Product-Name', 'Product-CostPrice', 'Product-SalesTax', 'Product-FinalPrice', 'Country']

        csv_writer = csv.DictWriter(newnew,fieldnames=fieldnames,delimiter = '\t')
        csv_writer.writeheader()

        for hehe in for_new:
            csv_writer.writerow(hehe)



calcprice(sales_tax,file_name,for_new)
    
